# SMARTdenovo singularity recipe

A Singularity recipe to create an image which wrap the SMARTdenovo assembly tools.

We use the last version on the github because they don't provide stable releases.

## Get it

### From the recipe

```bash
	sudo singularity build SMARTdenovo.simg Singularity
```

### Get the image from repositories

Will be available soon on singularity-hub. We are waiting for a compatibility with gitlab.

## Use it

### launch miasm

```bash
	./SMARTdenovo.simg [SMARTdenovo options] <input fasta>
```

## Links

* [SMARTdenovo](https://github.com/ruanjue/smartdenovo)
* [Singularity](https://www.sylabs.io/)
